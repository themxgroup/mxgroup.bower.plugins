"use strict";
angular.module("MxGroup.Shared", []);

angular.module("MxGroup.Shared")
	.directive("mxDropdown", function() {
		return {
			link: function(scope, elem, attrs, ctrl) {
				elem.dropdown({
					initialization: {
						cssClass	: attrs.initializationClass,
						delay		: attrs.initializationDelay
					},
					click: {
						cssClass	: attrs.clickClass,
						delay		: attrs.clickDelay
					},
					hover: {
						cssClass	: attrs.hoverClass,
						delay		: attrs.hoverDelay
					}
				});
			}
		}
	});

angular.module("MxGroup.Shared")
	.directive("noTransitions", function() {
		return {
			restrict: "C",
			link: function(scope, elem, attrs, ctrl) {
				$(document).ready(function() {
					elem.removeClass("no-transitions");
				});
			}
		}
	});

angular.module("MxGroup.Shared")
	.directive("mxSubmitStatus", ["$timeout", function($timeout) {
		return {
			link: function(scope, element, attrs, controller) {
				var scopeSelector = attrs.mxSubmitStatus;
				if(!scopeSelector)
					scopeSelector = element.parents("form:first").attr("name") + ".$submitting";

				var promise = null;
				scope.$watch(scopeSelector, function(isLoading) {
					isLoading = isLoading || false;

					$timeout.cancel(promise);
					promise = $timeout(function() {
						element.toggleClass("loading", isLoading);
						element.prop("disabled", isLoading);
					}, 100);
				});
			}
		};
	}]);

angular.module("MxGroup.Shared")
	.directive("contenteditable", ["$sce", function($sce) {
		// Taken from https://docs.angularjs.org/api/ng/type/ngModel.NgModelController

		return {
			restrict: "A",
			require: "?ngModel",
			link: function(scope, element, attrs, ngModel) {
				// Do nothing if no ng-model
				if(!ngModel)
					return;

				// Specify how UI should be updated
				ngModel.$render = function() {
					element.html($sce.getTrustedHtml(ngModel.$viewValue || ""));
				};

				// Listen for change events to enable binding
				element.on("blur keyup change", function() {
					scope.$apply(read);
				});

				// Initialize
				read();

				// Write data to the model
				function read() {
					var html = element.html();
					// When we clear the content editable the browser leaves a <br> behind
					// If strip-br attribute is provided then we strip this out
					if(attrs.stripBr && html == "<br />")
						html = "";

					ngModel.$setViewValue(html);
				}
			}
		};
	}]);

angular.module("MxGroup.Shared")
	.directive("form", ["$q", "$timeout", function($q, $timeout) {
		return {
			restrict: "E",
			require: "form",
			compile: function(tElement, tAttrs) {
				tElement.attr("novalidate", "");

				return {
					pre: function(scope, elem, attrs, ctrl) {
						elem
							.on("submit", function(e) {
								// Prevent submission if no action is defined
								if(tAttrs.action === undefined)
									e.preventDefault();

								// If the validation has already run, prevent additional validation
								// call, and allow normal processing
								if(ctrl.$validationCompleted)
									return;

								// Otherwise, prevent the normal processing and run any scheduled validation
								e.stopImmediatePropagation();
								e.preventDefault();

								ctrl.$submitted = true;
								ctrl.$submitting = true;
								elem.addClass("ng-submitted");
								elem.find(":file[required]").each(function() {
									var hasValue = ($(this).val() !== "");
									$(this).controller("ngModel")
										.$setValidity("required", hasValue);
								});
								scope.$apply();

								// Run any registered validators
								var promise = ctrl
									.validate()
									.then(function() {
										// Prevent the form from being submitted if invalid
										if(ctrl.$invalid)
											return;

										// Re-trigger submission if everything is a go.
										ctrl.$validationCompleted = true;
										$timeout(function() {
											elem.submit();
											ctrl.$validationCompleted = false;
										});
									});

								promise["finally"](function() {
									ctrl.$submitting = false;
								});
							});

						angular.extend(ctrl, {
							validators: [],
							registerValidator: function(validationMethod) {
								ctrl.validators.push(validationMethod);
							},
							validate: function() {
								if(ctrl.$invalid)
									return $q.when(false);

								var promises = [];
								ctrl.validators.forEach(function(validator) {
									promises.push($q.when(validator()));
								});

								return $q.all(promises);
							},
							reset: function() {
								elem.removeClass("ng-submitted");
								ctrl.$setPristine(true);
								ctrl.$submitted = false;
								ctrl.$submitting = false;
								ctrl.$validationCompleted = false;
								elem[0].reset();
							}
						});
					}
				}
			}
		}
	}]);

angular.module("MxGroup.Shared")
	.directive("mxToggle", function() {
		return {
			link: function(scope, elem, attrs, ctrl) {
				var toggleNames	= attrs.mxToggle.split(",");
				var toggleClass	= attrs.toggleClass || "selected";

				toggleNames.forEach(function(toggle) {
					scope.$on("mxToggle." + toggle, function(e, action) {
						var addClass;
						switch(action) {
							case "add"		: addClass = true; 	break;
							case "remove"	: addClass = false; break;
							default			: addClass = !elem.hasClass(toggleClass); break;
						}

						elem.toggleClass(toggleClass, addClass);
					});
				});
			}
		};
	});

angular.module("MxGroup.Shared")
	.directive("mxTogglerFor", ["$rootScope", function($rootScope) {
		return {
			link: function(scope, elem, attrs, ctrl) {
				var toggles = attrs.mxTogglerFor.split(",");

				elem.on("click", function(e) {
					if(e.originalEvent) {
						if(e.originalEvent.togglerHandled)
							return;

						e.originalEvent.togglerHandled = true;
					} else {
						e.stopPropagation();
					}

					toggles.forEach(function(toggle) {
						var action = "toggle";
						if(toggle[0] === "+")
							action = "add";
						else if(toggle[0] === "-")
							action = "remove";

						if(action !== "toggle")
							toggle = toggle.substring(1).trim();

						$rootScope.$broadcast("mxToggle." + toggle, action);
					});
				});
			}
		};
	}]);

angular.module("MxGroup.Shared")
	.directive("mxValidateEmail", ["$compile", function($compile) {
		return {
			priority: 100,
			terminal: true,
			compile: function(tElement, tAttrs) {
				tElement.removeAttr("mx-validate-email");
				tElement.attr("ng-pattern", /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
				return {
					post: function postLink(scope, elem, attrs, controller) {
						$compile(elem)(scope);
					}
				};
			}
		}
	}]);

angular.module("MxGroup.Shared")
	.directive("mxValidateMatch", function() {
		return {
			require: "ngModel",
			link: function(scope, elem, attrs, ctrl) {
				var otherValue = "";

				scope.$watch(attrs.mxValidateMatch, function(newValue) {
					otherValue = newValue;
					validate();
				});

				elem.bind("keyup", function() {
					scope.$apply(validate);
				});
				function validate() {
					var isValid = ctrl.$modelValue === undefined
									|| ctrl.$modelValue.length === 0
									|| otherValue === undefined
									|| otherValue == ctrl.$modelValue;

					ctrl.$setValidity("mismatch", isValid);
				}
			}
		};
	});

angular.module("MxGroup.Shared")
	.directive("mxValidationMessage", ["$compile", function($compile) {
		var wrapperShowTemplate = "<%= formName %>['<%= fieldName %>'].$invalid && (<%= formName %>['<%= fieldName %>'].$touched || <%= formName %>.$submitted)";
		var messageShowTemplate = "<%= formName %>['<%= fieldName %>'].$error.<%= errorName %>";

		return {
			compile: function(elem, attrs) {
				var messages	= [];
				var formName	= attrs.mxValidationMessage.split(".")[0];
				var fieldName	= attrs.mxValidationMessage.replace(formName + ".", "");

				var wrapper		= angular.element(document.createElement("span"));
				wrapper.attr("ng-show", wrapperShowTemplate
											.replace(/<%= formName %>/g, formName)
											.replace(/<%= fieldName %>/g, fieldName));

				for(var errorKey in attrs) {
					if(errorKey.indexOf("for") !== 0)
						continue;

					var errorName	= errorKey.replace(/^for/, "").toLowerCase();
					var errorMsg	= attrs[errorKey];
					var $msg = angular.element(document.createElement("span"))
										.text(errorMsg)
										.attr("ng-show", messageShowTemplate
															.replace(/<%= formName %>/g, formName)
															.replace(/<%= fieldName %>/g, fieldName)
															.replace(/<%= errorName %>/g, errorName));

					wrapper.append($msg);
				}

				elem.append(wrapper);
			}
		};
	}]);
