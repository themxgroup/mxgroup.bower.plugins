(function($) {
	$.fn.toMenu = function() {
		var $navLinks	= $(this).find("a").not("[href]");
		var menuID		= $(this).selector;

		$navLinks.click(function() {
			var $this		= $(this);
			var $navigation	= $this.next("ul");
			var storageKey	= menuID + $this.text();

			window.localStorage[storageKey] = !$navigation.is(":visible");

			$navigation.slideToggle();
		});
	};
})(jQuery);
