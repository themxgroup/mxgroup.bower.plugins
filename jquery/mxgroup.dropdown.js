(function($) {
	$.fn.dropdown = function(options) {
		var settings = $.extend(true, {
			initialization: {
				cssClass	: "mx-dropdown-current",
				delay: 0
			},
			click: {
				cssClass	: "mx-dropdown-click",
				delay		: 0
			},
			hover: {
				cssClass	: "mx-dropdown-hover",
				delay		: 150
			}
		}, options);
		settings.mouseover 	= settings.hover;
		settings.mouseout 	= settings.hover;

		$(this).each(function() {
			var $menu = $(this);

			$menu.initialize = function() {
				this.find("a").each(function() {
					if(location.href !== this.href)
						return;

					var $menuItem = $(this).parent("li");
					$menu.markSelectedMenuItem($menuItem, settings.initialization);
				});

				return this;
			};
			$menu.markSelectedMenuItem = function(menuItem, eventSettings) {
				$menuItem 		= $(menuItem);
				var $toOpen		= $menuItem.parentsUntil(this, "li").addBack();
				var $toClose	= this.find("li." + eventSettings.cssClass).not($toOpen);

				clearTimeout(this.data("open-timeout"));
				this.data("open-timeout", setTimeout(function() {
					$toClose.removeClass(eventSettings.cssClass);
					$toOpen.addClass(eventSettings.cssClass);
				}, eventSettings.delay));
			};

			$menu.initialize()
				.on("click mouseover mouseout", "li", function(e) {
					var $menuItem = $(this);

					switch(e.type) {
						case "mouseover":
							break;
						case "mouseout":
							$menuItem = null;
							break;
						default:
							if($menuItem.hasClass(settings[e.type].cssClass))
								$menuItem = $menuItem.parentsUntil($menu, "li:first");
							break;
					}

					e.stopPropagation();
					e.stopImmediatePropagation();
					$menu.markSelectedMenuItem($menuItem, settings[e.type]);
				});

			$(document).on("click", function() {
				$menu.markSelectedMenuItem(null, settings.click);
			});
		});

		return this;
	};
})(jQuery);
