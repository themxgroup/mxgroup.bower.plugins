(function($) {
	$.fn.tooltip = function(options) {
		var settings = $.extend({
			tooltipSide		: "top",
			helpSide		: "right",
			helpClass		: "help",
			activeClass		: "active",
			tooltipClass	: "info",
			tooltipID		: "ms-tooltip",
			messageClass	: "message",
			tooltipOptions	: { }
		}, options);
		settings.tooltipOptions = $.extend({
			dataAttribute	: "help",
			mode			: "direct",
			closeClass		: "close",
			openOnClick		: false
		}, settings.tooltipOptions);

		var $existingTooltip = $("#" + settings.tooltipID);
		if($existingTooltip.length === 0) {
			var $container = $("<div>");
			var $message = $("<p>");
			$message.addClass(settings.messageClass);
			if(settings.tooltipOptions.openOnClick) {
				var $closeButton = $("<a>").addClass(settings.tooltipOptions.closeClass)
												.css("cursor", "pointer")
												.text("X");
				$container.append($closeButton);
			}
			$container.attr("id", settings.tooltipID);
			$container.append($message);
			$container.hide();
			$("body").append($container);
		}

		$existingTooltip = $("#" + settings.tooltipID);
		var $tooltip = $existingTooltip.clone();
		$existingTooltip.remove();
		$tooltip.css("position", "absolute");
		$tooltip.hide();
		$("body").append($tooltip);

		settings.allElements = this;
		settings.allElements.each(function() {
			var $this = $(this);
			var entryEvent, exitEvent;

			if($this.is(":input")) {
				entryEvent	= "focus";
				exitEvent	= "blur";
			} else {
				if(!settings.tooltipOptions.openOnClick) {
					entryEvent	= "mouseenter";
					exitEvent	= "mouseleave";
				} else {
					entryEvent	= "click";
				}
			}

			$this.bind(entryEvent, function() {
				settings.allElements.removeClass(settings.activeClass);
				var location, typeClass;
				if($this.is(":input")) {
					location = settings.helpSide;
					typeClass = settings.helpClass;
				}
				else {
					location = settings.tooltipSide;
					typeClass = settings.tooltipClass;
				}

				var tooltipText = $this.data(settings.tooltipOptions.dataAttribute);
				if(settings.tooltipOptions.mode == "target")
					tooltipText = $(tooltipText).html();

				$tooltip.removeClass();
				$tooltip.find("." + settings.messageClass).html(tooltipText);
				$tooltip.addClass(location);
				$tooltip.addClass(typeClass);

				var targetTop		= $this.offset().top;
				var targetLeft		= $this.offset().left;
				var targetHeight	= $this.outerHeight();
				var targetWidth		= $this.outerWidth();
				var tooltipHeight	= $tooltip.outerHeight();
				var tooltipWidth	= $tooltip.outerWidth();
				var top, left;

				if(location == "top" || location == "bottom") {
					left = (targetLeft + (targetWidth / 2) - (tooltipWidth / 2));
					if(location == "top")
						top	= targetTop - tooltipHeight - 3;
					else if(location == "bottom")
						top	= targetTop + targetHeight + 3;
				}
				if(location == "left" || location == "right") {
					top	= targetTop + (targetHeight / 2) - (tooltipHeight / 2);
					if(location == "left")
						left = targetLeft - tooltipWidth - 3;
					else if(location == "right")
						left = targetLeft + targetWidth + 3;
				}

				$tooltip.css("top", top + "px");
				$tooltip.css("left", left + "px");
				$tooltip.show();
				$this.addClass(settings.activeClass);
			});
			if($this.is(":input") || !settings.tooltipOptions.openOnClick) {
				$this.bind(exitEvent, function() {
					$tooltip.hide();
					$this.removeClass(settings.activeClass);
				});
			} else {
				$tooltip.find("." + settings.tooltipOptions.closeClass).click(function() {
					$tooltip.hide();
					$this.removeClass(settings.activeClass);
				});
			}
		});
	};
})(jQuery);