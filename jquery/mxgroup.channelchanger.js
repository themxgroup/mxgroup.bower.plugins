var MarketSense = MarketSense || {};

(function($) {
	$.fn.channelchanger = function(settings) {
		if(typeof(this.data("changer")) !== "undefined")
			return this;

		this.each(function() {
			var $changer = $(this);
			$changer.data("changer", new MarketSense.ChannelChanger($changer, settings));
		});

		return this;
	};
	$.fn.getChanger = function() {
		return this.data("changer");
	};

	// == Channel Changer =====================================================
	// ========================================================================
	var ChannelChanger = MarketSense.ChannelChanger = function(container, settings) {
		this.nextRotation	= null;
		this.currentPane	= null;
		this.container		= container;
		this.settings		= this.processSettings(settings);
		this.panes			= this.container.find(this.settings.pane);
		this.pages			= this.container.find(this.settings.page);
		this.nextNav		= this.container.find(this.settings.next);
		this.prevNav		= this.container.find(this.settings.prev);

		var animationSettings = {
			duration: this.settings.animationDuration,
			onComplete: $.proxy(function() {
				this.currentPane = this.panes.filter(":visible:first");
			}, this)
		};
		this.animator = new MarketSense.Animator(animationSettings);

		this.initialize();
	};
	// == Initialization ======================================================
	ChannelChanger.prototype.processSettings = function(settings) {
		var fullSettings = $.extend({
			onChange				: function(){},
			page					: ".navigation .page",
			next					: ".navigation .next",
			prev					: ".navigation .prev",
			pane					: ".panes .pane",
			activePaneClass			: "active",
			activePageClass			: "active",
			disabledNavigationClass	: "disabled",
			autorotate				: false,
			initialDelay			: 0,
			durationSelector		: "duration",
			duration				: 7,
			wrap					: false,
			stopOnNavigate			: false,
			stopOnHover				: false,
			video					: { },
			animation				: "fade",
			animationDuration		: 400,
			startingPaneKey			: "p"
		}, settings);

		fullSettings.videoEnabled = (typeof $().fpvideo !== "undefined");
		fullSettings.video = $.extend({
			selector: "[video]",
			autoplay: true
		}, fullSettings.video);

		return fullSettings;
	};
	ChannelChanger.prototype.initialize = function() {
		var urlParser		= new UrlParser(location.href);
		var startingIndex	= parseInt(urlParser.get(this.settings.startingPaneKey), 10) || 1;
		startingIndex		= this.verifyIndex(startingIndex - 1);

		this.panes.hide();
		this.currentPane = this.panes.eq(startingIndex);
		this.currentPane.show();
		this.setEvents();
		this.updateNavigation(startingIndex);

		if(this.settings.videoEnabled)
			this.initializeVideos();

		var hasVideo = this.currentPane.find(this.settings.video.selector).length > 0;
		if(hasVideo)
			this.render(startingIndex);
		else if(this.settings.autorotate)
			this.play();
	};
	ChannelChanger.prototype.setEvents = function() {
		var self = this;
		var settings = this.settings;

		if(settings.stopOnHover) {
			this.container
				.on("mouseenter", settings.pane, function() { self.stop(); })
				.on("mouseleave", settings.pane, function() { self.play(); });
		}

		// Click of the specific page button
		this.container.on("click", settings.page, function(e) {
			var index = self.pages.index(this);
			self.goToPage(index);
			if(self.settings.stopOnNavigate)
				self.stop();
		});

		// Click of the previous button
		this.container.on("click", settings.prev + ":not(" + settings.disabledNavigationClass + ")", function() {
			self.prev();
		});
		// Click of the next button
		this.container.on("click", settings.next + ":not(" + settings.disabledNavigationClass + ")", function() {
			self.next();
		});
	};
	// == Navigation ==========================================================
	ChannelChanger.prototype.goToPage = function(index, direction) {
		index = this.verifyIndex(index);
		this.render(index, direction);
	};
	ChannelChanger.prototype.prev = function() {
		var currentIndex = this.panes.index(this.currentPane);
		this.goToPage(currentIndex - 1, -1);
	};
	ChannelChanger.prototype.next = function() {
		var currentIndex = this.panes.index(this.currentPane);
		this.goToPage(currentIndex + 1, 1);
	};
	// == Play Control ========================================================
	ChannelChanger.prototype.play = function(delay) {
		this.settings.autorotate = true;
		delay = delay || this.settings.duration;

		clearTimeout(this.nextRotation);
		this.nextRotation = setTimeout($.proxy(this.next, this), delay * 1000);

		if(!this.settings.wrap) {
			if(this.nextNav.hasClass(this.settings.disabledNavigationClass))
				this.stop();
		}
	};
	ChannelChanger.prototype.stop = function() {
		this.settings.autorotate = false;
		clearTimeout(this.nextRotation);
	};
	// == Rendering ===========================================================
	ChannelChanger.prototype.render = function(targetIndex, direction) {
		var currentIndex = this.panes.filter(":visible:first").index();
		if(this.panes.is(":animated") || currentIndex === targetIndex)
			return;

		var oldPane = this.panes.eq(currentIndex);
		this.animatePanes(currentIndex, targetIndex, direction);
		this.updateNavigation(targetIndex);
		var newPane = this.panes.eq(targetIndex);
		var videoFound = this.resetVideos(oldPane, newPane);

		if(this.settings.autorotate && !videoFound) {
			var delay = newPane.attr(this.settings.durationSelector) || this.settings.duration;
			this.play(delay);
		}
	};
	ChannelChanger.prototype.animatePanes = function(currentIndex, targetIndex, direction) {
		var animation = this.settings.animation;
		direction = direction || this.getAnimationDirection(currentIndex, targetIndex);
		if(animation === "slideHorizontal") {
			animation = (direction > 0) ? "slideLeft" : "slideRight";
		}
		else if(animation === "slideVertical") {
			animation = (direction > 0) ? "slideUp" : "slideDown";
		}

		var oldPane = this.panes.eq(currentIndex);
		var newPane = this.panes.eq(targetIndex);

		oldPane.removeClass(this.settings.activePaneClass);
		newPane.addClass(this.settings.activePaneClass);
		this.animator[animation](oldPane, newPane);
		this.settings.onChange(newPane, oldPane, this);
	};
	ChannelChanger.prototype.getAnimationDirection = function(currentIndex, targetIndex) {
		if(targetIndex > currentIndex)
			return 1;
		else if(targetIndex < currentIndex || this.panes.length === 2)
			return -1;
		else return 1;
	};
	ChannelChanger.prototype.updateNavigation = function(targetIndex) {
		this.pages.removeClass(this.settings.activePageClass);
		this.pages.eq(targetIndex).addClass(this.settings.activePageClass);

		// Update the prev/next class
		if(!this.settings.wrap) {
			var hasNext = (targetIndex < this.panes.length - 1);
			var hasPrev = (targetIndex > 0);

			this.nextNav.toggleClass(this.settings.disabledNavigationClass, !hasNext);
			this.prevNav.toggleClass(this.settings.disabledNavigationClass, !hasPrev);
		}
	};
	// == Videos ==============================================================
	ChannelChanger.prototype.initializeVideos = function() {
		var oldVideoSettings = $.extend({}, this.settings.video);

		this.settings.video.onLoad = $.proxy(function() {
			if(oldVideoSettings.onLoad) oldVideoSettings.onLoad();
			if(this.getClip().autoPlay)
				this.play();
		}, this);
		this.settings.video.onFinish = $.proxy(function() {
			if(this.settings.autorotate) {
				if(oldVideoSettings.onFinish) oldVideoSettings.onFinish();
				clearTimeout(this.nextRotation);
				this.nextRotation = setTimeout($.proxy(this.next, this), 1000);
			}
		}, this);
		this.settings.video.onPause = $.proxy(function() {
			if(this.settings.autorotate) {
				if(oldVideoSettings.onPause) oldVideoSettings.onPause();
				clearTimeout(this.nextRotation);
				this.nextRotation = setTimeout($.proxy(this.next, this), this.settings.duration * 1000);
			}
		}, this);
		this.settings.video.onBegin = $.proxy(function() {
			if(this.settings.autorotate) {
				if(oldVideoSettings.onBegin) oldVideoSettings.onBegin();
				clearTimeout(this.nextRotation);
			}
		}, this);
		this.settings.video.onResume = $.proxy(function() {
			if(this.settings.autorotate) {
				if(oldVideoSettings.onResume) oldVideoSettings.onResume();
				clearTimeout(this.nextRotation);
			}
		}, this);

		this.container.find(this.settings.video.selector).fpvideo(this.settings.video);
	};
	ChannelChanger.prototype.resetVideos = function(oldPane, newPane) {
		// Detect and play any videos
		var videoFound = false;
		if(this.settings.videoEnabled) {
			var oldVideo = oldPane.find(this.settings.video.selector);
			var newVideo = newPane.find(this.settings.video.selector);
			videoFound = (newVideo.length > 0 && newVideo.find("img").length === 0);
			oldVideo.stop();
			oldVideo.resetVideo();
		}

		// Reset animated gifs
		newPane.find("img[src$='.gif']").each(function() {
			this.src = this.src;
		});

		return videoFound;
	};
	// == Other ===============================================================
	ChannelChanger.prototype.verifyIndex = function(index) {
		if(this.settings.wrap) {
			return index % this.panes.length;
		}
		else {
			if(index <= 0)
				return 0;
			else if(index >= this.panes.length)
				return this.panes.length - 1;
			else return index;
		}
	};

	// == Animator ============================================================
	// ========================================================================
	var Animator = MarketSense.Animator = function(settings) {
		this.directions	= {
			up		: new Direction("top",	"negative"),
			down	: new Direction("top",	"positive"),
			left	: new Direction("left",	"negative"),
			right	: new Direction("left",	"positive")
		};
		this.settings = $.extend({
			duration	: 500,
			onComplete	: function() { }
		}, settings);

		function Direction(positioner, shiftSign) {
			this.positioner		= positioner;
			this.isPositive		= (shiftSign === "positive");
			this.isHorizontal	= (positioner === "left");
			this.action			= (this.isPositive) ? "+=" : "-=";

			this.getShift = function(pane) {
				return (this.isHorizontal) ? pane.outerWidth() : pane.outerHeight();
			};
		}
	};
	// Animations
	Animator.prototype.none = function(oldPane, newPane) {
		oldPane.hide();
		newPane.show();
		this.settings.onComplete();

		return this;
	};
	Animator.prototype.fade = function(oldPane, newPane) {
		oldPane.fadeOut(this.settings.duration, $.proxy(function() {
			newPane.fadeIn(this.settings.duration, $.proxy(function() {
				this.settings.onComplete();
			}, this));
		}, this));

		return this;
	};
	Animator.prototype.crossfade = function(oldPane, newPane) {
		this.containPane(newPane);

		newPane.css({
			"position"	: "absolute",
			"z-index"	: 1
		});
		oldPane.css({
			"position"	: "absolute",
			"z-index"	: 2
		});

		newPane.show();
		oldPane.fadeOut(this.settings.duration, $.proxy(function() {
			oldPane.css("z-index", 1);
			this.settings.onComplete();
		}, this));

		return this;
	};
	Animator.prototype.slideUp = function(oldPane, newPane) {
		return this.slide(oldPane, newPane, this.directions.up);
	};
	Animator.prototype.slideDown = function(oldPane, newPane) {
		return this.slide(oldPane, newPane, this.directions.down);
	};
	Animator.prototype.slideLeft = function(oldPane, newPane) {
		return this.slide(oldPane, newPane, this.directions.left);
	};
	Animator.prototype.slideRight = function(oldPane, newPane) {
		return this.slide(oldPane, newPane, this.directions.right);
	};
	// Helpers
	Animator.prototype.slide = function(oldPane, newPane, slideDirection) {
		this.prePositionPane(newPane, slideDirection);
		this.movePane(oldPane, slideDirection, true);
		this.movePane(newPane, slideDirection, false);

		return this;
	};
	Animator.prototype.movePane = function(pane, slideDirection, hideOnEnd) {
		var distance	= slideDirection.getShift(pane);
		var animation	= {};
		animation[slideDirection.positioner] = slideDirection.action + distance + "px";

		this.containPane(pane);
		pane.show();
		pane.animate(animation, this.settings.duration, $.proxy(function() {
			if(hideOnEnd)
				pane.hide();
			else this.settings.onComplete();
		}, this));
	};
	Animator.prototype.prePositionPane = function(pane, slideDirection) {
		var position = slideDirection.getShift(pane);
		if(slideDirection.isPositive)
			position *= -1;

		this.containPane(pane);
		pane.css(slideDirection.positioner, position + "px");
	};
	Animator.prototype.containPane = function(pane) {
		pane.css("position", "absolute");
		pane.parent().css({
			"overflow"	: "hidden",
			"position"	: "relative",
			"width"		: pane.width() + "px",
			"height"	: pane.height() + "px"
		});
	};

	// == Url Parser ==========================================================
	// ========================================================================
	var UrlParser = MarketSense.UrlParser = function(url) {
		var query = [];
		var urlPieces = url.split("?");
		if(urlPieces.length > 1) {
			urlPieces = urlPieces[1].split("&");
			for(var ct = 0; ct < urlPieces.length; ct++) {
				var kvp = urlPieces[ct].split("=");
				if(kvp.length === 1)
					continue;

				var key = kvp[0];
				var value = kvp[1];
				query[key] = value;
			}
		}

		this.get = function(key) {
			return query[key];
		};
	};
})(jQuery);
