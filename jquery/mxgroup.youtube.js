(function($) {
	function onStateChanged(state) {
		var action = "";
		switch(state.data) {
			case 0: action = "Ended"; break;
			case 1:	action = "Playing"; break;
			case 2:	action = "Paused"; break;
			case 3:	action = "Buffering"; break;
			case 5:	action = "Cued"; break;
			default: return;
		}

		if(typeof(_gaq) !== "undefined")
			_gaq.push(["_trackEvent", "Video", action, state.target.getVideoUrl()]);
	}

	$.fn.youtube = function(settings) {
		var $self = $(this);
		if($self.length === 0)
			return;

		settings = $.extend({
			height	: 390,
			width	: 640,
			playerVars: {},
			onReady	: function() {}
		}, settings);

		$.getScript("//www.youtube.com/player_api", function() {
			var interval = setInterval(function() {
				if(typeof(YT.Player) !== "undefined") {
					$self.each(function(index, item) {
						var $video		= $(item);
						var videoId		= $video.attr("data-videoId");
						var elementId	= $video.attr("id") || "ytplayer" + index;
						if(!videoId)
							return;

						$video.attr("id", elementId);
						var video = new YT.Player(elementId, {
							videoId	: videoId,
							height	: settings.height,
							width	: settings.width,
							playerVars: settings.playerVars
						});

						video.addEventListener("onStateChange", onStateChanged);

						settings.onReady(videoId, video);
					});

					clearInterval(interval);
				}
			}, 200);
		});

		return this;
	};
})(jQuery);

