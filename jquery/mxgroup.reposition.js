(function($) {
	$.fn.reposition = function(left, top) {
		var $target		= $(this);
		var width		= $target.outerWidth();
		var height		= $target.outerHeight();
		var rightEdge	= width + left;
		var bottomEdge	= height + top - $(window).scrollTop();

		if(rightEdge > $(window).width())
			left = left - width;
		if(bottomEdge > $(window).height())
			top = top - height;

		$target.css("position", "absolute");
		$target.css("left", left);
		$target.css("top", top);
	};
})(jQuery);