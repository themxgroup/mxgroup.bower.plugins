(function($) {
	$.fn.tooltip = function(options) {
		var $window	= $(window);
		var $body	= $("body");

		options = $.extend({
			tooltipID		: "ms-tooltip",
			messageClass	: "message",
			activeClass		: "active",
			defaultPosition	: "right middle",
			gap				: 5,
			fadeDuration	: 200
		}, options);
		options.messageSelector = "." + options.messageClass;
		options.tooltipIDSelector = "#" + options.tooltipID;

		var $tooltip = initializeTooltip(options);

		this.each(function() {
			var $tipped = $(this);
			var $existingMessage = $tipped.find(options.messageSelector);
			if($existingMessage.length === 0) {
				var $message = $tipped.contents()
									.wrapAll("<div>")
									.parent()
									.addClass(options.messageClass);

				$tipped.empty().append($message);
			}
		});

		$body.on("mouseenter", this.selector, function() {
			var $hovered		= $(this);
			var $currentTarget	= $tooltip.data("target");
			$tooltip.stop();

			// Do nothing if it is the same target
			if($hovered.hasClass(options.activeClass) && $hovered.is($currentTarget)) {
				$tooltip.css({ opacity: 1 });
				return;
			}

			// Update the current target
			$tooltip.data("target", $hovered);

			// Update the tooltip with the new contents
			var $contents = $hovered.find(options.messageSelector).clone();
			$tooltip.empty();
			$tooltip.append($contents);

			var hoverFrame = new Frame({
				top		: $hovered.offset().top,
				left	: $hovered.offset().left,
				height	: $hovered.outerHeight(),
				width	: $hovered.outerWidth()
			});
			var windowFrame = new Frame({
				top		: $window.scrollTop(),
				left	: $window.scrollLeft(),
				height	: $window.height(),
				width	: $window.width()
			});
			var tooltipFrame = new Frame({
				top		: hoverFrame.top(),
				left	: hoverFrame.left() + hoverFrame.width() + options.gap,
				height	: $tooltip.outerHeight(),
				width	: $tooltip.outerWidth()
			});

			var positionResults = tooltipFrame.isInside(windowFrame);
			if(!positionResults.inside)
				tooltipFrame.reposition(positionResults, hoverFrame, options.gap);

			$tooltip.css({
					top	: tooltipFrame.top() + "px",
					left: tooltipFrame.left() + "px",
					opacity: 1
				})
				.addClass(options.activeClass)
				.show();
		});
		$body.on("mouseenter", options.tooltipIDSelector, function() {
			$tooltip.stop().css({ opacity: 1 });
		});
		$body.on("mouseleave", options.tooltipIDSelector + ", " + this.selector, function() {
			$tooltip.fadeOut(options.fadeDuration);
		});

		return this;
	};

	function initializeTooltip(options) {
		$(options.tooltipIDSelector).remove();
		var $tooltip = $("<div>").attr("id", options.tooltipID)
									.css("position", "absolute")
									.hide();

		$("body").append($tooltip);

		return $tooltip;
	}

	var Frame = function(position) {
		this.position = position;
	};
	Frame.prototype.top = function() {
		return this.position.top;
	};
	Frame.prototype.bottom = function() {
		return this.position.top + this.position.height;
	};
	Frame.prototype.left = function() {
		return this.position.left;
	};
	Frame.prototype.right = function() {
		return this.position.left + this.position.width;
	};
	Frame.prototype.width = function() {
		return this.position.width;
	};
	Frame.prototype.height = function() {
		return this.position.height;
	};
	Frame.prototype.isInside = function(boundingFrame) {
		var results = {
			insideTop	: this.top() > boundingFrame.top(),
			insideBottom: this.bottom() < boundingFrame.bottom(),
			insideLeft	: this.left() > boundingFrame.left(),
			insideRight	: this.right() < boundingFrame.right()
		};
		results.inside = results.insideTop
							&& results.insideBottom
							&& results.insideLeft
							&& results.insideRight;
		return results;
	};
	Frame.prototype.reposition = function(positionResults, targetFrame, gap) {
		if(positionResults.inside)
			return;

		if(!positionResults.insideRight)
			this.position.left = targetFrame.left() - (this.width() + gap);
		if(!positionResults.insideBottom)
			this.position.top = targetFrame.top() - (this.height() + gap);
	};
})(jQuery);
